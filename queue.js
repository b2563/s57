let collection = [];

// Write the queue functions below.

function print() {
	// output all the elements of the queue
	return collection
}

function enqueue(element) {
	// add an element to the rear of the queue 
	for(let i = 0; i <= collection.length; i++) {
		if(i === collection.length) {
			collection[i] = element
			return collection
		}
	}

}

function dequeue() {
	// remove an element at the front of the queue
	let [, ...elements] = collection;
	collection = elements
	return collection

}

function front() {
	// show the element at the front of the queue
	return collection[0]
}

function size() {
	// show the total number of elements  
	let queueSize = collection.length
	return queueSize

}

function isEmpty() {
	// return a Boolean value describing whether the queue is empty or not
	if(collection === null) {
		return true
	} else {
			return false
	}

}

module.exports = {
	collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};